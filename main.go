package main

import (
	"booking-app/helper"
	"fmt"
	"sync"
	"time"
)

const conferenceTickets int = 50            //can't change
var conferenceName string = "Go conference" //can change
var remainingTickets uint = 50
var bookings = make([]UserData, 0)

type UserData struct {
	firstName       string
	lastName        string
	email           string
	numberOfTickets uint
}

var wg = sync.WaitGroup{} //initialize for goroutine syncronization

func main() {

	// fmt.Printf("conferenceTickets is % T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

	greetUsers()

	// for {

	// input
	firstName, lastName, email, userTickets := getUserInput()

	// validation
	isValidName, isValidEmail, isValidTicketNumber := helper.ValidationUserInput(firstName, lastName, email, userTickets, remainingTickets)

	if remainingTickets == 0 {
		fmt.Println("Our conference is booked out, Come back next year. Thank you for support us :)")
		// break
		// continue
	}

	// check available ticket
	if isValidName && isValidEmail && isValidTicketNumber {

		bookTicket(userTickets, firstName, lastName, email)

		wg.Add(1) //Add(n) : add waitGroup + 1, n appropriate sum of go func()
		go sendTicket(userTickets, firstName, lastName, email)

		// take first name
		firstName2 := getFirstName()
		fmt.Printf("First name bookings are : %v\n", firstName2)

		fmt.Println("===End Tickets Reservation===")
		fmt.Println("")

	} else {
		if !isValidName {
			fmt.Println("First name or last name you entered is too sort!")
		} else if !isValidEmail {
			fmt.Println("Your email address does't contain @ signature!")
		} else if !isValidTicketNumber {
			fmt.Printf("We only have %v tickets remaining, so you can't book %v tickets\n", remainingTickets, userTickets)
		}

		// fmt.Println("Your input data is invalid, please try again")
		// fmt.Printf("We only have %v tickets remaining, so you can't book %v tickets\n", remainingTickets, userTickets)
	}

	wg.Wait() //Wait() for wait WaitGroup to '0' (initialize on add())
}

// }

func greetUsers() {
	fmt.Printf("Welcome to %v booking application", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available. \n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")
}

func getFirstName() []string {
	firstName1 := []string{}
	for _, booking := range bookings {
		// var names = strings.Fields(booking) //strings.Fields : for separate string to comma slice. example: "Nicole "
		firstName1 = append(firstName1, booking.firstName)
	}
	return firstName1
}

func getUserInput() (string, string, string, uint) {
	// var bookings [50]string
	var firstName string
	var lastName string
	var email string
	var userTickets uint
	// var city string

	fmt.Println("===Tickets Reservation===")

	fmt.Print("Enter your first name : ")
	fmt.Scan(&firstName)

	fmt.Print("Enter your last name : ")
	fmt.Scan(&lastName)

	fmt.Print("Enter your email : ")
	fmt.Scan(&email)

	// fmt.Print("Enter your city (Singapore or London) : ")
	// fmt.Scan(&city)

	fmt.Print("Enter your user tickets : ")
	fmt.Scan(&userTickets)

	return firstName, lastName, email, userTickets
}

func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	remainingTickets = remainingTickets - userTickets

	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	bookings = append(bookings, userData)

	fmt.Printf("List of bookings is %v\n", bookings)

	// fmt.Printf("the whole array: %v\n", bookings)
	// fmt.Printf("the first value: %v\n", bookings[0])
	// fmt.Printf("Array type: %v\n", bookings)
	// fmt.Printf("Array length: %v\n", len(bookings))

	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v \n", firstName, lastName, userTickets, email)
	fmt.Printf("%v tickets remaining for %v \n", remainingTickets, conferenceName)
}

func sendTicket(userTickets uint, firstName string, lastName string, email string) {

	time.Sleep(10 * time.Second)

	ticket := fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	fmt.Println("\n======Tickets=======")
	fmt.Printf("Sending ticket:\n %v \nto email address %v", ticket, email)
	fmt.Println("\n===End of Tickets===\n")
	wg.Done() //Done() : decrement the waitgroup (-1)
}
