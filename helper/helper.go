package helper

import (
	"strings"
)

func ValidationUserInput(firstName string, lastName string, email string, userTickets uint, remainingTickets uint) (bool, bool, bool) {
	isValidName := len(firstName) >= 2 && len(lastName) >= 2
	isValidEmail := strings.Contains(email, "@")
	isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets
	// isValidCity := strings.ToUpper(city) == "SINGAPORE" || strings.ToUpper(city) == "LONDON"

	return isValidName, isValidEmail, isValidTicketNumber
}
